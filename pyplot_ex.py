import matplotlib.pyplot as plt
import xarray as xr

remote_dataset = xr.open_dataset('http://thredds.atmos.albany.edu:8080/thredds/dodsC/ERA-Interim/2018/tsfc.2018.nc').sel(time='2018-03-01T00:00:00').load()

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
plot = ax.contourf(remote_dataset["tsfc"], cmap="inferno")
fig.colorbar(plot, orientation="horizontal", label="Temperature (K)", shrink=0.8)
fig.savefig("figure.png")
plt.show()
plt.close(fig)
